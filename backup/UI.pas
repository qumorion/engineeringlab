﻿unit UI;

interface

uses
  CRT,
  MatrixUnit;

const
  NumberOfMenuItems = 9;
  XCoordinateOfMenu = 10;
  YCoordinateOfMenu = 5;

var
  menuItems: array [1..NumberOfMenuItems] of string;
  selectedMenuItem: byte;
  pressedButton: char;

procedure WriteWelcome;
procedure WriteMenu(xCoordinate, yCoordinate: byte);
procedure SetSelectedMenuItemColor(xCoordinate, yCoordinate: byte);
procedure RestoreMenuItemColor(xCoordinate, yCoordinate: byte);
procedure StartMenu(var workingMatrix: SquareMatrix);

implementation

procedure WriteWelcome;
begin
  TextColor(blue);
  TextBackground(LightGray);
  ClrScr;
  GotoXY(XCoordinateOfMenu, YCoordinateOfMenu);
  WriteLn('Добро пожаловать в программу для работы с квадратными матрицами');
  GotoXY(XCoordinateOfMenu, YCoordinateOfMenu + 1);
  Write('Чтобы приступить к работе нажмите [ENTER]');
  ReadLn;
end;

procedure WriteMenu(xCoordinate, yCoordinate: byte);
var
  i: byte;
begin
  TextBackground(LightGray);
  ClrScr;
  TextColor(Black);
  for i := 1 to NumberOfMenuItems do
  begin
    GotoXY(xCoordinate, yCoordinate + i);
    Write(menuItems[i]);
  end;
end;

procedure SetSelectedMenuItemColor(xCoordinate, yCoordinate: byte);
begin
  TextColor(Blue);
  GotoXY(xCoordinate, yCoordinate);
  Write(menuItems[selectedMenuItem]);
end;


procedure RestoreMenuItemColor(xCoordinate, yCoordinate: byte);
begin
  TextColor(Black);
  GotoXY(xCoordinate, yCoordinate);
  Write(menuItems[selectedMenuItem]);
end;

procedure StartMenu(var workingMatrix: SquareMatrix);
var
  size: integer;
begin
  WriteWelcome;
  WriteMenu(XCoordinateOfMenu, YCoordinateOfMenu);
  SetSelectedMenuItemColor(XCoordinateOfMenu, YCoordinateOfMenu + selectedMenuItem);
  while pressedButton <> #27 do //escape
  begin
    pressedButton := ReadKey;
    if pressedButton = #0 then
    begin
      pressedButton := ReadKey;
      case pressedButton of
        #80:
        begin //стрелка вниз
          if selectedMenuItem < NumberOfMenuItems then
          begin
            RestoreMenuItemColor(XCoordinateOfMenu, YCoordinateOfMenu +
              selectedMenuItem);
            Inc(selectedMenuItem);
            SetSelectedMenuItemColor(XCoordinateOfMenu, YCoordinateOfMenu +
              selectedMenuItem);
          end;
        end;
        #72: //стрелка вверх
        begin
          if selectedMenuItem > 1 then
          begin
            RestoreMenuItemColor(XCoordinateOfMenu, YCoordinateOfMenu +
              selectedMenuItem);
            Dec(selectedMenuItem);
            SetSelectedMenuItemColor(XCoordinateOfMenu, YCoordinateOfMenu +
              selectedMenuItem);
          end;
        end;
      end;
    end
    else if pressedButton = #13 then //enter
    begin
      case selectedMenuItem of
        1:
        begin
          GenerateMatrix(workingMatrix);

        end;
        2:
        begin
          WriteMatrix(workingMatrix);
          readln;
        end;

        3:
        begin
          ClrScr;
          WriteLn('Введите новую размерность квадратной матрицы: ');
          ReadLn(size);
          if size > 0 then
            ChangeSizeMatrix(workingMatrix, size)
          else
          begin
            ClrScr;
            WriteLn('Введённые данные некорректны');
            ReadLn;
          end;
          GenerateMatrix(workingMatrix);
        end;
        4:
        begin
          ChangeRangeOfRandomValues(workingMatrix.minRandomValue,
            workingMatrix.maxRandomValue);
          GenerateMatrix(workingMatrix);
        end;
        5:
        begin
          ClrScr;
          WriteMatrix(testMatrix);
          GotoXY(XCoordinateOfMenu, WhereY + 3);
          WriteLn('Минимальная сумма элементов столбцов: ',
            GetMinSumForColumn(workingMatrix));
          ReadKey;
        end;
        6:
        begin
          ClrScr;
          WriteMatrix(testMatrix);
          GotoXY(XCoordinateOfMenu, WhereY + 3);
          WriteWhereIsMaxElement(workingMatrix);
          readln;
        end;
        7:
        begin
          ClrScr;
          WriteMatrix(testMatrix);
          GotoXY(XCoordinateOfMenu, WhereY + 3);
          WriteLn(
            'Количество строк, все элементы которых отрицательны: ',
            GetNumberOfNegativeStrings(workingMatrix));
          ReadKey;
        end;
        8:
        begin
          pressedButton := #27;   //escape
        end;
        9:
        begin
          ClrScr;
          GotoXY(XCoordinateOfMenu, WhereY + 3);
          FillMatrixFromConsole(testMatrix);
          writeln('Матрица успешно введена');
          ReadKey;
        end;
      end;
      WriteMenu(XCoordinateOfMenu, YCoordinateOfMenu);
      SetSelectedMenuItemColor(XCoordinateOfMenu, YCoordinateOfMenu + selectedMenuItem);
    end;
  end;
end;


begin
  selectedMenuItem := 1;
  menuItems[1] := 'Генерирование матрицы';
  menuItems[2] := 'Просмотр матрицы';
  menuItems[3] := 'Изменить размерность матрицы';
  menuItems[4] := 'Изменить диапазон элементов матрицы';
  menuItems[5] :=
    'Найти минимальную сумму элементов столбцов';
  menuItems[6] :=
    'Определить позицию первого максимального элемента матрицы';
  menuItems[7] :=
    'Определить количество строк матрицы с отрицательными элементами';
  menuItems[8] := 'Выход из программы';
  menuItems[9] := 'Ввести матрицу из консоли';
end.

