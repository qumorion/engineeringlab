﻿unit MatrixUnit;

interface

uses
  CRT, SysUtils, Classes;

type
  SquareMatrix = record
    Data: array of array of integer;
    minRandomValue, maxRandomValue: integer;
  end;

var
  testMatrix: SquareMatrix;

procedure GenerateMatrix(var workingMatrix: SquareMatrix);
procedure WriteMatrix(workingMatrix: SquareMatrix);
procedure ChangeSizeMatrix(var workingMatrix: SquareMatrix; size: integer);
procedure ChangeRangeOfRandomValues(var minRangeLimit, maxRangeLimit: integer);
function GetMinSumForColumn(var workingMatrix: SquareMatrix): string;
procedure FillMatrixFromConsole(var workingMatrix: SquareMatrix);
procedure WriteWhereIsMaxElement(var workingMatrix: SquareMatrix);
function GetNumberOfNegativeStrings(var workingMatrix: SquareMatrix): integer;

implementation

procedure GenerateMatrix(var workingMatrix: SquareMatrix);
var
  i, j: integer;
begin
  ClrScr;
  Randomize;
  for i := 0 to High(workingMatrix.Data) do
    for j := 0 to High(workingMatrix.Data) do
    begin
      workingMatrix.Data[i, j] :=
        Random(workingMatrix.maxRandomValue - workingMatrix.minRandomValue + 1) +
        workingMatrix.minRandomValue;
    end;
  WriteLn('Новая матрица размерностью ',
    Length(workingMatrix.Data), 'x', Length(workingMatrix.Data),
    ' с числами из диапазона [',
    workingMatrix.minRandomValue, ' ; ', workingMatrix.maxRandomValue,
    '] сгенерирована');
  ReadLn;
end;

procedure WriteMatrix(workingMatrix: SquareMatrix);
var
  i, j: integer;
begin
  ClrScr;
  for i := 0 to High(workingMatrix.Data) do
  begin
    for j := 0 to High(workingMatrix.Data) do
    begin
      Write(workingMatrix.Data[i, j]: 5);
      Write(' ');
    end;
    WriteLn();
  end;
end;

procedure ChangeSizeMatrix(var workingMatrix: SquareMatrix; size: integer);
var
  i: integer;
begin
  SetLength(workingMatrix.Data, size);
  for i := 0 to size - 1 do
    SetLength(workingMatrix.Data[i], size);
end;

procedure ChangeRangeOfRandomValues(var minRangeLimit, maxRangeLimit: integer);
var
  min, max, swap: integer;
begin
  ClrScr;
  WriteLn('Введите минимальное значение диапазона: ');
  ReadLn(min);
  WriteLn;
  WriteLn('Введите максимальное значение диапазона: ');
  ReadLn(max);
  if min > max then
  begin
    swap := min;
    min := max;
    max := swap;
  end;
  minRangeLimit := min;
  maxRangeLimit := max;
end;

function GetMinSumForColumn(var workingMatrix: SquareMatrix): string;
var
  i, j, minSum, currentSum, minColumn: integer;
begin
  currentSum := 0;
  minSum := 0;
  minColumn := 1;
  for j := 0 to High(workingMatrix.Data) do
  begin
    minSum += workingMatrix.Data[j, 0];
  end;
  for i := 1 to high(workingMatrix.Data) do
  begin
    currentSum := 0;
    for j := 0 to High(workingMatrix.Data[i]) do
    begin
      currentSum += workingMatrix.Data[j, i];
    end;
    if (minSum > currentSum) then
    begin
      minSum := currentSum;
      minColumn := i + 1;
    end;
  end;
  Result := IntToStr(minSum) + ' столбец ' + IntToStr(minColumn);
end;


procedure WriteWhereIsMaxElement(var workingMatrix: SquareMatrix);
var
  i, j, maxElement: integer;
  isBelow, isOn, isAbove: boolean;
begin
  maxElement := workingMatrix.Data[0, 0];
  isBelow := False;
  isOn := False;
  isAbove := False;
  for i := 0 to High(workingMatrix.Data) do
  begin
    for j := 0 to High(workingMatrix.Data) do
    begin
      if (maxElement < workingMatrix.Data[i, j]) then
      begin
        maxElement := workingMatrix.Data[i, j];
      end;
    end;
  end;

  for i := 0 to High(workingMatrix.Data) do
  begin
    for j := 0 to High(workingMatrix.Data) do
    begin
      if (maxElement = workingMatrix.Data[i, j]) then
      begin
        if ((i + j + 1) = Length(testMatrix.Data)) then
          isOn := True
        else
        if ((i + j + 1) < Length(testMatrix.Data)) then
          isAbove := True
        else

          isBelow := True;
      end;
    end;
  end;


  if (isOn) then
    writeln('Максимальный элемент (', maxElement,
      ') находится на побочной диагонали');
  GotoXY(10, WhereY);
  if (isAbove) then
    writeln('Максимальный элемент (', maxElement,
      ') находится над побочной диагональю');
  GotoXY(10, WhereY);
  if (isBelow) then
    writeln('Максимальный элемент (', maxElement,
      ') находится под побочной диагональю');
end;

function GetNumberOfNegativeStrings(var workingMatrix: SquareMatrix): integer;
var
  i, j, Count: integer;
begin
  Count := 0;
  for i := 0 to High(workingMatrix.Data) do
  begin
    for j := 0 to High(workingMatrix.Data) do
    begin
      if (workingMatrix.Data[i, j] >= 0) then
        break;
    end;
    if (j = High(workingMatrix.Data)) then
      Count += 1;
  end;
  Result := Count;
end;


procedure FillMatrixFromConsole(var workingMatrix: SquareMatrix);
var
  i, j, k: integer;
  splitter: TStringList;
  consoleInput: string;
begin
  splitter := TStringList.Create;
  Writeln('Введите матрицу размера ',
    Length(workingMatrix.Data), ' на ', Length(workingMatrix.Data));
  for i := 0 to High(workingMatrix.Data) do
  begin
    while (True) do
    begin
      readln(consoleInput);
      consoleInput := Trim(consoleInput);
      splitter.DelimitedText := consoleInput;
      if (splitter.Count < length(workingMatrix.Data)) then
        writeln('Введено недостаточное количество цифр, попробуйте снова')
      else
        break;
    end;
    for j := 0 to High(workingMatrix.Data) do
      workingMatrix.data[i, j] := StrToInt(splitter[j]);
  end;
end;

begin
  testMatrix.minRandomValue := -10;
  testMatrix.maxRandomValue := 10;
  ChangeSizeMatrix(testMatrix, 17);
end.
